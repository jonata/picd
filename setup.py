#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#######################################################################
#
# Picd
#
#######################################################################

from setuptools import setup

from helpers import SetupHelpers
import picd

# --------------------------------------------------------------------------- #

setup_requires = ['setuptools']
install_requires = [
    'pyscreenshot',
    'pyscreenshot @ git+https://github.com/jonata/pyscreenshot.git@freedesktop-portal',
    'requests',
    'PySide6',
    'pynput',
    'Pillow',
    'pyinstaller'
]

# --------------------------------------------------------------------------- #

try:
    # begin setuptools installer
    result = setup(
        name=picd.__appname__.lower(),
        version=picd.__version__,
        author=picd.__author__,
        author_email=picd.__email__,
        description='picd is a screenshot tool for the desktop',
        long_description=SetupHelpers.get_description(),
        url=picd.__website__,
        license='GPL3',
        packages=['picd', 'picd.modules'],
        setup_requires=setup_requires,
        install_requires=install_requires,
        data_files=SetupHelpers.get_data_files(),
        package_data=SetupHelpers.get_package_files(),
        include_package_data=True,
        entry_points={
            'console_scripts': [
                'picd = picd.__main__:cmd',
            ],
            'gui_scripts': [
                'picd = picd.__main__:call_gui'
            ]
        },
        keywords='picd',
        classifiers=[
            'Development Status :: 5 - Production/Stable',
            'Environment :: X11 Applications :: Qt',
            'Intended Audience :: End Users/Desktop',
            'License :: GPL3',
            'Natural Language :: English',
            'Operating System :: POSIX',
            'Topic :: Office',
            'Programming Language :: Python :: 3 :: Only'
        ]
    )
except BaseException:
    if picd.__ispypi__:
        SetupHelpers.pip_notes()
    raise
