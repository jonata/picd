# -*- mode: python -*-

VERSION = '20.11.0.0'
if 'VERSION_NUMBER' in [*os.environ] and not os.environ['VERSION_NUMBER'] == '':
    VERSION = os.environ['VERSION_NUMBER']

block_cipher = None

a = Analysis(['picd.py'],
             pathex=['/home/jonata/Projetos/picd'],
             binaries=[],
             datas=[
                     ( 'picd/graphics/*', 'graphics' ),
                 ],
             hiddenimports=['PySide2', 'pynput', 'pyscreenshot', 'requests', 'Pillow'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='picd',
          debug=False,
          strip=False,
          upx=True,
          console=True )

coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='picd')

open('dist/picd/current_version', mode='w', encoding='utf-8').write(VERSION)
