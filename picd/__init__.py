#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#######################################################################
#
# Prat
#
#######################################################################

__version__ = '22.04.01.01'

__appname__ = 'picd'
__domain__ = 'picd.jonata.org'
__desktopid__ = 'org.jonata.picd'
# __appid__ = ''

__author__ = 'Jonatã Bolzan Loss'
__email__ = 'admin@picd.in'
__website__ = 'https://picd.in'
__bugreport__ = 'https://gitlab.com/jonata/picd/issues'

__ispypi__ = False
