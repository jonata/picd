#!/usr/bin/python3

import argparse
from picd.modules.paths import ACTUAL_OS

parser = argparse.ArgumentParser(description='If no argument, picd will just take a screenshot of all screens and upload it to picd.in')
parser.add_argument('--systray', help='Shows the system tray icon', action='store_true')
parser.add_argument('--about', help='Shows the about window', action='store_true')
parser.add_argument('--version', help='Prints the version of picd app.', action='store_true')
args = parser.parse_args()


def call_gui(fire_on_init=False):
    from picd.modules import config
    # Check it it will open on startup
    if config.CONFIG['systray'].get('auto_startup', True):
        config.check_autostart(config=config.CONFIG)

    from picd.modules import gui

    show_about_window = args.about
    gui.systray(show_about_window=show_about_window, fire_on_init=fire_on_init)

    config.save(config=config.CONFIG)


def cmd():
    from picd.modules import screenshot
    screenshot_filepath = screenshot.fire()

    if screenshot_filepath:
        from picd.modules import upload
        response = upload.upload_to_picdin(filepath=screenshot_filepath)
        if response and 'public_url' in response.json():
            print(response.json()['public_url'])
            from picd.modules.config import CONFIG
            if CONFIG['browser'].get('open_url_in_browser', True):
                import webbrowser
                webbrowser.open(response.json()['public_url'])

if args.systray or (ACTUAL_OS == 'windows'):
    call_gui(fire_on_init=(ACTUAL_OS == 'windows' and not args.systray))
else:
    cmd()
