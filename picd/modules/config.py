#!/usr/bin/env python3

import os
import json

from picd.modules.paths import PATH_PICD_USER_CONFIG_FILE, ACTUAL_OS, PATH_HOME


def load():
    config = {}
    if os.path.isfile(PATH_PICD_USER_CONFIG_FILE):
        with open(PATH_PICD_USER_CONFIG_FILE) as f:
            config = json.load(f)

    if not config.get('systray', False):
        config['systray'] = {}

    if not config.get('browser', False):
        config['browser'] = {}

    if not config.get('clipboard', False):
        config['clipboard'] = {}

    if not config.get('upload', False):
        config['upload'] = {}

    return config


def save(config=False):
    if config:
        with open(PATH_PICD_USER_CONFIG_FILE, 'wb') as f:
            f.write(json.dumps(config, indent=4, ensure_ascii=False).encode('utf-8'))


def check_autostart(config=False):
    if ACTUAL_OS == 'linux':
        if not os.path.isdir(os.path.join(PATH_HOME, '.config', 'autostart')):
            os.mkdir(os.path.join(PATH_HOME, '.config', 'autostart'))
        if not os.path.isfile(os.path.join(PATH_HOME, '.config', 'autostart', 'picd.desktop')):
            open(os.path.join(PATH_HOME, '.config', 'autostart', 'picd.desktop'), 'w').write('[Desktop Entry]\nName=picd\nExec=picd --systray')


CONFIG = load()
