#!/usr/bin/env python3

import os
import sys
import webbrowser
import shutil
import datetime
from math import sqrt

from PySide6.QtGui import QCursor, QIcon, QPixmap, QPainter, QBrush, QColor, QPen, QLinearGradient, QPolygonF, QPainterPath, QFont, QClipboard, QImage, QAction, QGuiApplication
from PySide6.QtWidgets import QHBoxLayout, QMenu, QApplication, QSizePolicy, QSlider, QSystemTrayIcon, QWidget, QLabel, QVBoxLayout, QCheckBox, QPushButton, QFileDialog, QGraphicsPixmapItem, QGraphicsBlurEffect, QGraphicsScene
from PySide6.QtCore import Qt, QRect, QPointF, QSize

from picd.modules.paths import PATH_PICD_GRAPHICS, VERSION_NUMBER, ACTUAL_OS, PATH_TMP, REAL_PATH_HOME, get_graphics_path
from picd.modules import screenshot
from picd.modules.config import CONFIG
from picd.modules import upload
from picd.modules.hotkeys import listen_to_hotkeys


MAIN_ICON_PATH = os.path.join(PATH_PICD_GRAPHICS, 'picd.png')

class fullscreen(QWidget):
    def __init__(self, app):
        super().__init__()
        self.setWindowTitle('picd fullscreen view')
        self.setWindowIcon(QIcon(MAIN_ICON_PATH))
        self.resize(QGuiApplication.screenAt(QCursor.pos()).size())

        self.app = app

        screen_displacement = [0, 0]
        if ACTUAL_OS == 'windows':
            for s in self.app.screens():
                if s.geometry().x() < screen_displacement[0]:
                    screen_displacement[0] += s.geometry().x()
                if s.geometry().y() < screen_displacement[1]:
                    screen_displacement[1] += s.geometry().y()

        self.move(0 + screen_displacement[0], 0 + screen_displacement[1])
        self.setWindowFlags(self.windowFlags() | Qt.WindowFullScreen | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint | Qt.X11BypassWindowManagerHint)

        self.setMouseTracking(True)

        stylesheet_text = ''

        for button_color in ['primary', 'secondary', 'red', 'silver', 'dark']:
            stylesheet_text += '#button_' + button_color + '                                { font-size:11px; border-left:5px; border-top:5px; border-right:5px; border-bottom:5px; border-image: url("' + get_graphics_path('button_' + button_color + '_normal.png') + '") 5 5 5 5 stretch stretch; outline: none; color:rgb(255,255,255); } '
            stylesheet_text += '#button_' + button_color + ':hover:pressed                  { font-size:11px; border-left:5px; border-top:5px; border-right:5px; border-bottom:5px; border-image: url("' + get_graphics_path('button_' + button_color + '_pressed.png') + '") 5 5 5 5 stretch stretch; outline: none;  color:rgb(255,255,255); } '
            stylesheet_text += '#button_' + button_color + ':checked                        { font-size:11px; border-left:5px; border-top:5px; border-right:5px; border-bottom:5px; border-image: url("' + get_graphics_path('button_' + button_color + '_pressed.png') + '") 5 5 5 5 stretch stretch; outline: none;  color:rgb(255,255,255); } '
            stylesheet_text += '#button_' + button_color + ':hover:checked                  { font-size:11px; border-left:5px; border-top:5px; border-right:5px; border-bottom:5px; border-image: url("' + get_graphics_path('button_' + button_color + '_pressed.png') + '") 5 5 5 5 stretch stretch; outline: none;  color:rgb(255,255,255); } '
            stylesheet_text += '#button_' + button_color + ':hover                          { font-size:11px; border-left:5px; border-top:5px; border-right:5px; border-bottom:5px; border-image: url("' + get_graphics_path('button_' + button_color + '_hover.png') + '") 5 5 5 5 stretch stretch; outline: none; color:rgb(255,255,255);  } '
            stylesheet_text += '#button_' + button_color + ':disabled                       { font-size:11px; border-left:5px; border-top:5px; border-right:5px; border-bottom:5px; border-image: url("' + get_graphics_path('button_' + button_color + '_disabled.png') + '") 5 5 5 5 stretch stretch; outline: none; color:rgba(255,255,255,100); }'

        self.setStyleSheet(stylesheet_text)
        self.setStyleSheet(open(os.path.join(PATH_PICD_GRAPHICS, 'fullscreen.qss')).read().replace('PATH_PICD_GRAPHICS/', get_graphics_path('_')[:-1]))

        # self.fullscreen_label = QLabel(self)
        self.screenshot_pixmap = QPixmap()
        self.black_filter_alpha = 150

        self.clip_rect = QRect(0, 0, 0, 0)
        self.clip_rect_smaller_side = 0
        self.corner = False
        self.list_of_annotations = []

        self.is_cursor_pressing = False

        self.tl_rect = QRect(0, 0, 0, 0)
        self.l_rect = QRect(0, 0, 0, 0)
        self.bl_rect = QRect(0, 0, 0, 0)
        self.b_rect = QRect(0, 0, 0, 0)
        self.br_rect = QRect(0, 0, 0, 0)
        self.r_rect = QRect(0, 0, 0, 0)
        self.tr_rect = QRect(0, 0, 0, 0)
        self.t_rect = QRect(0, 0, 0, 0)

        self.cursor_pos = [0, 0]
        self.cursor_margin = [0, 0]
        self.clip_rect_finished = False
        self.show_starting_message = True

        self.tool_widget = QWidget(parent=self)
        self.tool_widget.setObjectName('tool_widget')
        self.tool_widget.setFixedHeight(80)

        self.tool_widget_area = QVBoxLayout(self.tool_widget)
        self.tool_widget_area.setContentsMargins(0, 0, 0, 0)

        self.tool_widget_tools_main = QWidget(self.tool_widget)

        self.tool_widget_tools_main_hbox = QHBoxLayout(self.tool_widget_tools_main)
        self.tool_widget_tools_main_hbox.setContentsMargins(24, 6, 80, 1)

        self.tool_widget_tools_main_annotation_highlighter = QPushButton(parent=self.tool_widget_tools_main)
        self.tool_widget_tools_main_annotation_highlighter.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'highlighter.svg')))
        self.tool_widget_tools_main_annotation_highlighter.setIconSize(QSize(20, 20))
        self.tool_widget_tools_main_annotation_highlighter.setProperty('border', 'false')
        # self.tool_widget_tools_main_annotation_highlighter.setStyleSheet('QPushButton { padding-top:2px; border-top:0; border-right:0; border-left:0;}')
        self.tool_widget_tools_main_annotation_highlighter.clicked.connect(self.tool_widget_tools_main_annotation_highlighter_clicked)
        self.tool_widget_tools_main_hbox.addWidget(self.tool_widget_tools_main_annotation_highlighter)

        self.tool_widget_tools_main_annotation_square = QPushButton(parent=self.tool_widget_tools_main)
        self.tool_widget_tools_main_annotation_square.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'square.svg')))
        self.tool_widget_tools_main_annotation_square.setIconSize(QSize(20, 20))
        self.tool_widget_tools_main_annotation_square.setProperty('border', 'false')
        # self.tool_widget_tools_main_annotation_square.setObjectName('button_secondary')
        # self.tool_widget_tools_main_annotation_square.setStyleSheet('QPushButton { padding-top:2px; border-top:0; border-right:0;}')
        self.tool_widget_tools_main_annotation_square.clicked.connect(self.tool_widget_tools_main_annotation_square_clicked)
        self.tool_widget_tools_main_hbox.addWidget(self.tool_widget_tools_main_annotation_square)

        self.tool_widget_tools_main_annotation_filledsquare = QPushButton(parent=self.tool_widget_tools_main)
        self.tool_widget_tools_main_annotation_filledsquare.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'filledsquare.svg')))
        self.tool_widget_tools_main_annotation_filledsquare.setIconSize(QSize(20, 20))
        self.tool_widget_tools_main_annotation_filledsquare.setProperty('border', 'false')
        # self.tool_widget_tools_main_annotation_filledsquare.setStyleSheet('QPushButton { padding-top:2px; border-top:0; border-right:0; border-left:0;}')
        self.tool_widget_tools_main_annotation_filledsquare.clicked.connect(self.tool_widget_tools_main_annotation_filledsquare_clicked)
        self.tool_widget_tools_main_hbox.addWidget(self.tool_widget_tools_main_annotation_filledsquare)

        self.tool_widget_tools_main_annotation_circle = QPushButton(parent=self.tool_widget_tools_main)
        self.tool_widget_tools_main_annotation_circle.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'circle.svg')))
        self.tool_widget_tools_main_annotation_circle.setIconSize(QSize(20, 20))
        self.tool_widget_tools_main_annotation_circle.setProperty('border', 'false')
        # self.tool_widget_tools_main_annotation_circle.setStyleSheet('QPushButton { padding-top:2px; border-top:0; border-right:0; border-left:0;}')
        self.tool_widget_tools_main_annotation_circle.clicked.connect(self.tool_widget_tools_main_annotation_circle_clicked)
        self.tool_widget_tools_main_hbox.addWidget(self.tool_widget_tools_main_annotation_circle)

        self.tool_widget_tools_main_annotation_filledcircle = QPushButton(parent=self.tool_widget_tools_main)
        self.tool_widget_tools_main_annotation_filledcircle.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'filledcircle.svg')))
        self.tool_widget_tools_main_annotation_filledcircle.setIconSize(QSize(20, 20))
        self.tool_widget_tools_main_annotation_filledcircle.setProperty('border', 'false')
        self.tool_widget_tools_main_annotation_filledcircle.clicked.connect(self.tool_widget_tools_main_annotation_filledcircle_clicked)
        self.tool_widget_tools_main_hbox.addWidget(self.tool_widget_tools_main_annotation_filledcircle)

        self.tool_widget_tools_main_annotation_blur = QPushButton(parent=self.tool_widget_tools_main)
        self.tool_widget_tools_main_annotation_blur.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'blur.svg')))
        self.tool_widget_tools_main_annotation_blur.setIconSize(QSize(20, 20))
        self.tool_widget_tools_main_annotation_blur.setProperty('border', 'false')
        self.tool_widget_tools_main_annotation_blur.clicked.connect(self.tool_widget_tools_main_annotation_blur_clicked)
        self.tool_widget_tools_main_hbox.addWidget(self.tool_widget_tools_main_annotation_blur)

        self.tool_widget_tools_main_annotation_freehand = QPushButton(parent=self.tool_widget_tools_main)
        self.tool_widget_tools_main_annotation_freehand.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'freehand.svg')))
        self.tool_widget_tools_main_annotation_freehand.setIconSize(QSize(20, 20))
        self.tool_widget_tools_main_annotation_freehand.setProperty('border', 'false')
        self.tool_widget_tools_main_annotation_freehand.clicked.connect(self.tool_widget_tools_main_annotation_freehand_clicked)
        self.tool_widget_tools_main_hbox.addWidget(self.tool_widget_tools_main_annotation_freehand)

        self.tool_widget_tools_main_annotation_line = QPushButton(parent=self.tool_widget_tools_main)
        self.tool_widget_tools_main_annotation_line.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'line.svg')))
        self.tool_widget_tools_main_annotation_line.setIconSize(QSize(20, 20))
        self.tool_widget_tools_main_annotation_line.setProperty('border', 'false')
        # self.tool_widget_tools_main_annotation_line.setStyleSheet('QPushButton { padding-top:2px; border-top:0; border-right:0; border-left:0;}')
        self.tool_widget_tools_main_annotation_line.clicked.connect(self.tool_widget_tools_main_annotation_line_clicked)
        self.tool_widget_tools_main_hbox.addWidget(self.tool_widget_tools_main_annotation_line)

        self.tool_widget_tools_main_annotation_arrow = QPushButton(parent=self.tool_widget_tools_main)
        self.tool_widget_tools_main_annotation_arrow.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'arrow.svg')))
        self.tool_widget_tools_main_annotation_arrow.setIconSize(QSize(20, 20))
        self.tool_widget_tools_main_annotation_arrow.setProperty('border', 'false')
        self.tool_widget_tools_main_annotation_arrow.clicked.connect(self.tool_widget_tools_main_annotation_arrow_clicked)
        self.tool_widget_tools_main_hbox.addWidget(self.tool_widget_tools_main_annotation_arrow)

        self.tool_widget_area.addWidget(self.tool_widget_tools_main)

        self.tool_widget_tools_hue = QSlider(Qt.Horizontal, self.tool_widget)
        self.tool_widget_tools_hue.setVisible(False)
        self.tool_widget_area.addWidget(self.tool_widget_tools_hue)

        self.tool_widget_tools_options = QWidget(self.tool_widget)

        self.tool_widget_tools_options_hbox = QHBoxLayout(self.tool_widget_tools_options)
        self.tool_widget_tools_options_hbox.setContentsMargins(5, 1, 80, 6)

        self.tool_widget_is_being_moved = False
        self.tool_widget_position_user_changed = False

        class tool_widget_tools_options_move_butt(QLabel):
            def enterEvent(widget, event):
                widget.setPixmap(QPixmap(os.path.join(PATH_PICD_GRAPHICS, 'move_hover.svg')))

            def leaveEvent(widget, event):
                widget.setPixmap(QPixmap(os.path.join(PATH_PICD_GRAPHICS, 'move_normal.svg')))

            def mousePressEvent(widget, event):
                widget.setPixmap(QPixmap(os.path.join(PATH_PICD_GRAPHICS, 'move_pressed.svg')))
                widget.setCursor(QCursor(Qt.ClosedHandCursor))
                event.ignore()

            def mouseReleaseEvent(widget, event):
                widget.setPixmap(QPixmap(os.path.join(PATH_PICD_GRAPHICS, 'move_normal.svg')))
                widget.setCursor(QCursor(Qt.OpenHandCursor))
                event.ignore()

        self.tool_widget_tools_options_move_button = tool_widget_tools_options_move_butt(parent=self.tool_widget_tools_options)
        # self.tool_widget_tools_options_move_button.setObjectName('tool_widget_tools_options_move_button')
        # self.tool_widget_tools_options_move_button.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'move.svg')))
        self.tool_widget_tools_options_move_button.setPixmap(QPixmap(os.path.join(PATH_PICD_GRAPHICS, 'move_normal.svg')))
        self.tool_widget_tools_options_move_button.setCursor(QCursor(Qt.OpenHandCursor))
        # self.tool_widget_tools_options_move_button.setIconSize(QSize(42, 18))
        # self.tool_widget_tools_options_move_button.setProperty('border', 'false')
        self.tool_widget_tools_options_hbox.addWidget(self.tool_widget_tools_options_move_button, 0, Qt.AlignLeft)

        self.tool_widget_tools_options_hbox.addStretch()

        self.tool_widget_tools_options_cancel_button = QPushButton(parent=self.tool_widget_tools_options)
        self.tool_widget_tools_options_cancel_button.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'cancel.svg')))
        self.tool_widget_tools_options_cancel_button.setIconSize(QSize(20, 20))
        self.tool_widget_tools_options_cancel_button.setProperty('border', 'false')
        self.tool_widget_tools_options_cancel_button.clicked.connect(self.tool_widget_tools_options_cancel_button_clicked)
        self.tool_widget_tools_options_hbox.addWidget(self.tool_widget_tools_options_cancel_button, 0, Qt.AlignCenter)

        self.tool_widget_tools_options_copyclipboard_button = QPushButton(parent=self.tool_widget_tools_options)
        self.tool_widget_tools_options_copyclipboard_button.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'copy.svg')))
        self.tool_widget_tools_options_copyclipboard_button.setIconSize(QSize(20, 20))
        self.tool_widget_tools_options_copyclipboard_button.setProperty('border', 'false')
        self.tool_widget_tools_options_copyclipboard_button.clicked.connect(self.tool_widget_tools_options_copyclipboard_button_clicked)
        self.tool_widget_tools_options_hbox.addWidget(self.tool_widget_tools_options_copyclipboard_button)

        self.tool_widget_tools_options_upload_button = QPushButton(parent=self.tool_widget_tools_options)
        self.tool_widget_tools_options_upload_button.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'computing-cloud.svg')))
        self.tool_widget_tools_options_upload_button.setIconSize(QSize(20, 20))
        self.tool_widget_tools_options_upload_button.setProperty('border', 'false')
        self.tool_widget_tools_options_upload_button.clicked.connect(self.tool_widget_tools_options_upload_button_clicked)
        self.tool_widget_tools_options_hbox.addWidget(self.tool_widget_tools_options_upload_button)

        self.tool_widget_tools_options_download_button = QPushButton(parent=self.tool_widget_tools_options)
        self.tool_widget_tools_options_download_button.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'download.svg')))
        self.tool_widget_tools_options_download_button.setIconSize(QSize(20, 20))
        self.tool_widget_tools_options_download_button.setProperty('border', 'false')
        self.tool_widget_tools_options_download_button.clicked.connect(self.tool_widget_tools_options_download_button_clicked)
        self.tool_widget_tools_options_hbox.addWidget(self.tool_widget_tools_options_download_button)

        self.tool_widget_area.addWidget(self.tool_widget_tools_options)

        self.tool_widget_action_button = QPushButton(parent=self.tool_widget)
        self.tool_widget_action_button.setObjectName('tool_widget_action_button')
        self.tool_widget_action_button.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed))
        self.tool_widget_action_button.setFixedSize(QSize(80, 80))
        self.tool_widget_action_button.clicked.connect(self.tool_widget_action_button_clicked)

    def reset(self):
        self.clip_rect = QRect(0, 0, 0, 0)
        self.corner = False
        self.is_cursor_pressing = False
        self.cursor_margin = [0, 0]
        self.clip_rect_finished = False
        self.show_starting_message = True
        self.list_of_annotations = []
        self.tool_widget_position_user_changed = False
        self.update_controls_position()
        self.update()

    def update_controls_position(self, pos=False):
        self.tool_widget.setVisible(bool(self.tool_widget_is_being_moved) or (not bool(self.tool_widget_is_being_moved) and self.clip_rect_finished and not self.is_cursor_pressing))

        if bool(pos):
            self.tool_widget.move(pos[0], pos[1])
        elif not self.tool_widget_position_user_changed:
            if self.clip_rect.bottom() + 120 < self.height():
                self.tool_widget.move(self.clip_rect.right() - self.tool_widget.width(), self.clip_rect.bottom() + 20)
            else:
                self.tool_widget.move(self.clip_rect.right() - self.tool_widget.width() - 20, self.clip_rect.bottom() - self.tool_widget.height() - 20)

        self.tool_widget_action_button.move(self.tool_widget.width() - self.tool_widget_action_button.width(), 0)

    def get_qpixmap(self, rect):
        self.corner = False
        self.is_cursor_pressing = False
        self.update()
        self.tool_widget.setVisible(False)
        return self.grab(rect)

    def tool_widget_tools_options_copyclipboard_button_clicked(self):
        image = self.get_qpixmap(self.clip_rect)
        self.tool_widget_tools_options_cancel_button_clicked()
        self.app.clipboard().setImage(image.toImage(), QClipboard.Clipboard)

    def tool_widget_tools_options_upload_button_clicked(self):
        screenshot_filepath = os.path.join(PATH_TMP, 'selected.png')
        image = self.get_qpixmap(self.clip_rect)
        image.save(screenshot_filepath)
        self.tool_widget_tools_options_cancel_button_clicked()
        response = upload.upload_to_picdin(filepath=screenshot_filepath)
        if response and 'public_url' in response.json() and not self.app.fullscreen.isVisible():
            if CONFIG['browser'].get('open_url_in_browser', True):
                webbrowser.open(response.json()['public_url'])
                if CONFIG['clipboard'].get('copy_url_to_clipboard', True):
                    self.app.clipboard().setText(response.json()['public_url'])

    def tool_widget_tools_options_download_button_clicked(self):
        screenshot_filepath = os.path.join(PATH_TMP, 'selected.png')
        image = self.get_qpixmap(self.clip_rect)
        image.save(screenshot_filepath)
        self.tool_widget_tools_options_cancel_button_clicked()
        filepath_to_save = QFileDialog.getSaveFileName(None, 'Save image', os.path.join(REAL_PATH_HOME, 'screenshot{}.png'.format(datetime.datetime.now().strftime("%Y%m%d%H%M%S"))), 'PNG Image (*.png)')
        if filepath_to_save[0]:
            shutil.copyfile(screenshot_filepath, filepath_to_save[0])
        else:
            shutil.copyfile(screenshot_filepath, os.path.join(REAL_PATH_HOME, 'screenshot.png'))

    def tool_widget_tools_options_cancel_button_clicked(self):
        if self.isVisible():
            self.close()

    def tool_widget_tools_main_annotation_square_clicked(self):
        self.list_of_annotations.append(['square', False, False])

    def tool_widget_tools_main_annotation_filledsquare_clicked(self):
        self.list_of_annotations.append(['filledsquare', False, False])

    def tool_widget_tools_main_annotation_circle_clicked(self):
        self.list_of_annotations.append(['circle', False, False])

    def tool_widget_tools_main_annotation_filledcircle_clicked(self):
        self.list_of_annotations.append(['filledcircle', False, False])

    def tool_widget_tools_main_annotation_highlighter_clicked(self):
        self.list_of_annotations.append(['highlighter', False, False])

    def tool_widget_tools_main_annotation_freehand_clicked(self):
        self.list_of_annotations.append(['freehand', False, False])

    def tool_widget_tools_main_annotation_line_clicked(self):
        self.list_of_annotations.append(['line', False, False])

    def tool_widget_tools_main_annotation_arrow_clicked(self):
        self.list_of_annotations.append(['arrow', False, False])

    def tool_widget_action_button_clicked(self):
        self.tool_widget_tools_options_download_button_clicked()
        if CONFIG['upload'].get('upload_to_picdin_on_confirm', True):
            self.tool_widget_tools_options_upload_button_clicked()

    def tool_widget_tools_main_annotation_blur_clicked(self):
        image = self.get_qpixmap(self.rect())
        self.list_of_annotations.append(['blur', False, False, blur_qpixmap(image, 25)])

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setRenderHint(QPainter.SmoothPixmapTransform)
        painter.drawPixmap(0, 0, self.screenshot_pixmap)
        rect = QRect(0, 0, self.width(), self.height())
        painter.fillRect(rect, QBrush(QColor(0, 0, 0, self.black_filter_alpha)))
        painter.setClipRect(self.clip_rect)
        painter.drawPixmap(0, 0, self.screenshot_pixmap)
        painter.setClipping(False)

        if self.list_of_annotations:
            for annotation in self.list_of_annotations:
                if annotation[0] in ['arrow', 'line', 'freehand', 'blur', 'square', 'filledsquare', 'circle', 'filledcircle', 'highlighter']:
                    if not annotation[1]:
                        if annotation[0] in ['highlighter']:
                            rect = QRect(self.cursor_pos[0] - 12, self.cursor_pos[1] - 12, 24, 24)
                            painter.setBrush(QBrush(QColor.fromRgb(0, 255, 0, 40)))
                        else:
                            rect = QRect(self.cursor_pos[0] - 3, self.cursor_pos[1] - 3, 6, 6)
                            painter.setBrush(QBrush(QColor.fromRgb(255, 0, 0)))
                        painter.setPen(Qt.NoPen)
                        painter.drawEllipse(rect)

                    if annotation[1] and annotation[0] in ['arrow', 'line']:
                        if annotation[2]:
                            pen = QPen(QColor.fromRgb(255, 0, 0), 6, Qt.SolidLine)
                            pen.setCapStyle(Qt.RoundCap)
                            pen.setJoinStyle(Qt.MiterJoin)
                            painter.setPen(pen)
                            painter.drawLine(annotation[1][0], annotation[1][1], annotation[2][0], annotation[2][1])

                            if annotation[0] in ['arrow']:
                                painter.setBrush(Qt.NoBrush)
                                x1, y1 = annotation[2]
                                x2, y2 = annotation[1]
                                dx, dy = x1 - x2, y1 - y2
                                norm = sqrt(dx * dx + dy * dy)
                                udx, udy = dx / norm, dy / norm
                                ax = udx * sqrt(3) / 2 - udy * 1 / 2
                                ay = udx * 1 / 2 + udy * sqrt(3) / 2
                                bx = udx * sqrt(3) / 2 + udy * 1 / 2
                                by = - udx * 1 / 2 + udy * sqrt(3) / 2
                                polygon = QPolygonF()
                                polygon.append(QPointF(x1 - 20 * ax, y1 - 20 * ay))
                                polygon.append(QPointF(annotation[2][0], annotation[2][1]))
                                polygon.append(QPointF(x1 - 20 * bx, y1 - 20 * by))
                                painter.drawPolyline(polygon)

                    if annotation[0] in ['freehand']:
                        if annotation[1]:
                            painter.setBrush(Qt.NoBrush)
                            pen = QPen(QColor.fromRgb(255, 0, 0), 6, Qt.SolidLine)
                            pen.setCapStyle(Qt.RoundCap)
                            pen.setJoinStyle(Qt.RoundJoin)
                            painter.setPen(pen)
                            polygon = QPolygonF()
                            for point in annotation[1]:
                                polygon.append(QPointF(point[0], point[1]))
                            painter.drawPolyline(polygon)

                    if annotation[0] in ['blur']:
                        if annotation[1] and annotation[2]:
                            brect = QRect(annotation[1][0], annotation[1][1], (annotation[2][0] - annotation[1][0]), (annotation[2][1] - annotation[1][1]))
                            painter.setClipRect(brect)
                            painter.drawPixmap(0, 0, QPixmap.fromImage(annotation[-1]))
                            painter.setClipping(False)

                    if annotation[0] in ['square']:
                        if annotation[1] and annotation[2]:
                            painter.setBrush(Qt.NoBrush)
                            pen = QPen(QColor.fromRgb(255, 0, 0), 6, Qt.SolidLine)
                            pen.setCapStyle(Qt.RoundCap)
                            pen.setJoinStyle(Qt.RoundJoin)
                            painter.setPen(pen)
                            srect = QRect(annotation[1][0], annotation[1][1], (annotation[2][0] - annotation[1][0]), (annotation[2][1] - annotation[1][1]))
                            painter.drawRect(srect)

                    if annotation[0] in ['filledsquare']:
                        if annotation[1] and annotation[2]:
                            pen = QPen(QColor.fromRgb(255, 0, 0), 6, Qt.SolidLine)
                            pen.setCapStyle(Qt.RoundCap)
                            pen.setJoinStyle(Qt.RoundJoin)
                            painter.setPen(pen)
                            painter.setBrush(QBrush(QColor.fromRgb(255, 0, 0)))
                            fsrect = QRect(annotation[1][0], annotation[1][1], (annotation[2][0] - annotation[1][0]), (annotation[2][1] - annotation[1][1]))
                            painter.drawRect(fsrect)

                    if annotation[0] in ['circle']:
                        if annotation[1] and annotation[2]:
                            painter.setBrush(Qt.NoBrush)
                            pen = QPen(QColor.fromRgb(255, 0, 0), 6, Qt.SolidLine)
                            pen.setCapStyle(Qt.RoundCap)
                            pen.setJoinStyle(Qt.MiterJoin)
                            painter.setPen(pen)
                            crect = QRect(annotation[1][0], annotation[1][1], (annotation[2][0] - annotation[1][0]), (annotation[2][1] - annotation[1][1]))
                            painter.drawEllipse(crect)

                    if annotation[0] in ['filledcircle']:
                        if annotation[1] and annotation[2]:
                            pen = QPen(QColor.fromRgb(255, 0, 0), 6, Qt.SolidLine)
                            pen.setCapStyle(Qt.RoundCap)
                            pen.setJoinStyle(Qt.MiterJoin)
                            painter.setPen(pen)
                            painter.setBrush(QBrush(QColor.fromRgb(255, 0, 0)))
                            fcrect = QRect(annotation[1][0], annotation[1][1], (annotation[2][0] - annotation[1][0]), (annotation[2][1] - annotation[1][1]))
                            painter.drawEllipse(fcrect)

                    if annotation[0] in ['highlighter']:
                        if annotation[1] and annotation[2]:
                            pen = QPen(QColor.fromRgb(0, 255, 0, 40), 24, Qt.SolidLine)
                            pen.setCapStyle(Qt.RoundCap)
                            pen.setJoinStyle(Qt.MiterJoin)
                            painter.setPen(pen)
                            painter.drawLine(annotation[1][0], annotation[1][1], annotation[2][0], annotation[2][1])

        if self.clip_rect_smaller_side:
            self.tl_rect.setRect(self.clip_rect.x(),
                                    self.clip_rect.y(),
                                    self.clip_rect_smaller_side * .3,
                                    self.clip_rect_smaller_side * .3)
            if self.corner == 'tl':
                painter.setPen(Qt.NoPen)

                grad = QLinearGradient(self.tl_rect.x(), self.tl_rect.y(), self.tl_rect.center().x(), self.tl_rect.center().y())
                grad.setColorAt(0.0, QColor.fromRgb(0, 0, 0, 100))
                grad.setColorAt(1.0, QColor.fromRgb(0, 0, 0, 0))
                painter.setBrush(QBrush(grad))

                painter.drawRect(self.tl_rect)

                painter.setBrush(Qt.NoBrush)
                pen = QPen(QColor.fromRgb(255, 255, 255, 150), 10, Qt.SolidLine)
                pen.setCapStyle(Qt.RoundCap)
                pen.setJoinStyle(Qt.MiterJoin)
                painter.setPen(pen)

                polygon = QPolygonF()
                polygon.append(QPointF(self.tl_rect.x() + 20, self.tl_rect.center().y()))
                polygon.append(QPointF(self.tl_rect.x() + 20, self.tl_rect.top() + 20))
                polygon.append(QPointF(self.tl_rect.center().x(), self.tl_rect.top() + 20))
                path = QPainterPath()
                path.addPolygon(polygon)
                painter.drawPath(path)

            self.l_rect.setRect(self.clip_rect.x(),
                                self.clip_rect.y(),
                                self.clip_rect_smaller_side * .15,
                                self.clip_rect.height())
            if self.corner == 'l':
                painter.setPen(Qt.NoPen)

                grad = QLinearGradient(self.l_rect.x(), self.l_rect.y(), self.l_rect.right(), self.l_rect.y())
                grad.setColorAt(0.0, QColor.fromRgb(0, 0, 0, 100))
                grad.setColorAt(1.0, QColor.fromRgb(0, 0, 0, 0))
                painter.setBrush(QBrush(grad))

                painter.drawRect(self.l_rect)

                painter.setBrush(Qt.NoBrush)
                pen = QPen(QColor.fromRgb(255, 255, 255, 150), 10, Qt.SolidLine)
                pen.setCapStyle(Qt.RoundCap)
                pen.setJoinStyle(Qt.MiterJoin)
                painter.setPen(pen)

                polygon = QPolygonF()
                polygon.append(QPointF((self.l_rect.x() + (self.l_rect.width() * .5)) + 20, self.l_rect.center().y() - (self.l_rect.width() * .5)))
                polygon.append(QPointF(self.l_rect.left() + 20, self.l_rect.center().y()))
                polygon.append(QPointF((self.l_rect.x() + (self.l_rect.width() * .5)) + 20, self.l_rect.center().y() + (self.l_rect.width() * .5)))
                path = QPainterPath()
                path.addPolygon(polygon)
                painter.drawPath(path)

            self.bl_rect.setRect(self.clip_rect.x(),
                                    self.clip_rect.bottom() + 1,
                                    self.clip_rect_smaller_side * .3,
                                    -self.clip_rect_smaller_side * .3)
            if self.corner == 'bl':
                painter.setPen(Qt.NoPen)

                grad = QLinearGradient(self.bl_rect.x(), self.bl_rect.top(), self.bl_rect.center().x(), self.bl_rect.center().y())
                grad.setColorAt(0.0, QColor.fromRgb(0, 0, 0, 100))
                grad.setColorAt(1.0, QColor.fromRgb(0, 0, 0, 0))
                painter.setBrush(QBrush(grad))

                painter.drawRect(self.bl_rect)

                painter.setBrush(Qt.NoBrush)
                pen = QPen(QColor.fromRgb(255, 255, 255, 150), 10, Qt.SolidLine)
                pen.setCapStyle(Qt.RoundCap)
                pen.setJoinStyle(Qt.MiterJoin)
                painter.setPen(pen)

                polygon = QPolygonF()
                polygon.append(QPointF(self.bl_rect.x() + 20, self.bl_rect.center().y()))
                polygon.append(QPointF(self.bl_rect.x() + 20, self.bl_rect.top() - 20))
                polygon.append(QPointF(self.bl_rect.center().x(), self.bl_rect.top() - 20))
                path = QPainterPath()
                path.addPolygon(polygon)
                painter.drawPath(path)

            self.b_rect.setRect(self.clip_rect.x(),
                                self.clip_rect.bottom() + 1,
                                self.clip_rect.width(),
                                -self.clip_rect_smaller_side * .15)
            if self.corner == 'b':
                painter.setPen(Qt.NoPen)

                grad = QLinearGradient(self.b_rect.x(), self.b_rect.top(), self.b_rect.x(), self.b_rect.bottom())
                grad.setColorAt(0.0, QColor.fromRgb(0, 0, 0, 100))
                grad.setColorAt(1.0, QColor.fromRgb(0, 0, 0, 0))
                painter.setBrush(QBrush(grad))

                painter.drawRect(self.b_rect)

                painter.setBrush(Qt.NoBrush)
                pen = QPen(QColor.fromRgb(255, 255, 255, 150), 10, Qt.SolidLine)
                pen.setCapStyle(Qt.RoundCap)
                pen.setJoinStyle(Qt.MiterJoin)
                painter.setPen(pen)

                polygon = QPolygonF()
                polygon.append(QPointF((self.b_rect.center().x()) - (self.b_rect.height() * .5), self.b_rect.y() + (self.b_rect.height() * .5) - 20))
                polygon.append(QPointF(self.b_rect.center().x(), self.b_rect.top() - 20))
                polygon.append(QPointF((self.b_rect.center().x()) + (self.b_rect.height() * .5), self.b_rect.y() + (self.b_rect.height() * .5) - 20))
                path = QPainterPath()
                path.addPolygon(polygon)
                painter.drawPath(path)

            self.br_rect.setRect(self.clip_rect.right() + 1,
                                    self.clip_rect.bottom() + 1,
                                    -self.clip_rect_smaller_side * .3,
                                    -self.clip_rect_smaller_side * .3)
            if self.corner == 'br':
                painter.setPen(Qt.NoPen)

                grad = QLinearGradient(self.br_rect.left(), self.br_rect.top(), self.br_rect.center().x(), self.br_rect.center().y())
                grad.setColorAt(0.0, QColor.fromRgb(0, 0, 0, 100))
                grad.setColorAt(1.0, QColor.fromRgb(0, 0, 0, 0))
                painter.setBrush(QBrush(grad))

                painter.drawRect(self.br_rect)

                painter.setBrush(Qt.NoBrush)
                pen = QPen(QColor.fromRgb(255, 255, 255, 150), 10, Qt.SolidLine)
                pen.setCapStyle(Qt.RoundCap)
                pen.setJoinStyle(Qt.MiterJoin)
                painter.setPen(pen)

                polygon = QPolygonF()
                polygon.append(QPointF(self.br_rect.left() - 20, self.br_rect.center().y()))
                polygon.append(QPointF(self.br_rect.left() - 20, self.br_rect.top() - 20))
                polygon.append(QPointF(self.br_rect.center().x(), self.br_rect.top() - 20))
                path = QPainterPath()
                path.addPolygon(polygon)
                painter.drawPath(path)

            self.r_rect.setRect(self.clip_rect.right() + 1,
                                self.clip_rect.y(),
                                -self.clip_rect_smaller_side * .15,
                                self.clip_rect.height())
            if self.corner == 'r':
                painter.setPen(Qt.NoPen)

                grad = QLinearGradient(self.r_rect.left(), self.r_rect.y(), self.r_rect.right(), self.r_rect.y())
                grad.setColorAt(0.0, QColor.fromRgb(0, 0, 0, 100))
                grad.setColorAt(1.0, QColor.fromRgb(0, 0, 0, 0))
                painter.setBrush(QBrush(grad))

                painter.drawRect(self.r_rect)

                painter.setBrush(Qt.NoBrush)
                pen = QPen(QColor.fromRgb(255, 255, 255, 150), 10, Qt.SolidLine)
                pen.setCapStyle(Qt.RoundCap)
                pen.setJoinStyle(Qt.MiterJoin)
                painter.setPen(pen)

                polygon = QPolygonF()
                polygon.append(QPointF((self.r_rect.x() + (self.r_rect.width() * .5)) - 20, self.r_rect.center().y() - (self.r_rect.width() * .5)))
                polygon.append(QPointF(self.r_rect.left() - 20, self.r_rect.center().y()))
                polygon.append(QPointF((self.r_rect.x() + (self.r_rect.width() * .5)) - 20, self.r_rect.center().y() + (self.r_rect.width() * .5)))
                path = QPainterPath()
                path.addPolygon(polygon)
                painter.drawPath(path)

            self.tr_rect.setRect(self.clip_rect.right() + 1,
                                    self.clip_rect.y(),
                                    -self.clip_rect_smaller_side * .3,
                                    self.clip_rect_smaller_side * .3)
            if self.corner == 'tr':
                painter.setPen(Qt.NoPen)

                grad = QLinearGradient(self.tr_rect.left(), self.tr_rect.top(), self.tr_rect.center().x(), self.tr_rect.center().y())
                grad.setColorAt(0.0, QColor.fromRgb(0, 0, 0, 100))
                grad.setColorAt(1.0, QColor.fromRgb(0, 0, 0, 0))
                painter.setBrush(QBrush(grad))

                painter.drawRect(self.tr_rect)

                painter.setBrush(Qt.NoBrush)
                pen = QPen(QColor.fromRgb(255, 255, 255, 150), 10, Qt.SolidLine)
                pen.setCapStyle(Qt.RoundCap)
                pen.setJoinStyle(Qt.MiterJoin)
                painter.setPen(pen)

                polygon = QPolygonF()
                polygon.append(QPointF(self.tr_rect.left() - 20, self.tr_rect.center().y()))
                polygon.append(QPointF(self.tr_rect.left() - 20, self.tr_rect.top() + 20))
                polygon.append(QPointF(self.tr_rect.center().x(), self.tr_rect.top() + 20))
                path = QPainterPath()
                path.addPolygon(polygon)
                painter.drawPath(path)

            self.t_rect.setRect(self.clip_rect.x(),
                                self.clip_rect.y(),
                                self.clip_rect.width(),
                                self.clip_rect_smaller_side * .15)
            if self.corner == 't':
                painter.setPen(Qt.NoPen)

                grad = QLinearGradient(self.t_rect.left(), self.t_rect.top(), self.t_rect.left(), self.t_rect.bottom())
                grad.setColorAt(0.0, QColor.fromRgb(0, 0, 0, 100))
                grad.setColorAt(1.0, QColor.fromRgb(0, 0, 0, 0))
                painter.setBrush(QBrush(grad))

                painter.drawRect(self.t_rect)

                painter.setBrush(Qt.NoBrush)
                pen = QPen(QColor.fromRgb(255, 255, 255, 150), 10, Qt.SolidLine)
                pen.setCapStyle(Qt.RoundCap)
                pen.setJoinStyle(Qt.MiterJoin)
                painter.setPen(pen)

                polygon = QPolygonF()
                polygon.append(QPointF((self.t_rect.center().x()) - (self.t_rect.height() * .5), self.t_rect.y() + (self.t_rect.height() * .5) + 20))
                polygon.append(QPointF(self.t_rect.center().x(), self.t_rect.top() + 20))
                polygon.append(QPointF((self.t_rect.center().x()) + (self.t_rect.height() * .5), self.t_rect.y() + (self.t_rect.height() * .5) + 20))
                path = QPainterPath()
                path.addPolygon(polygon)
                painter.drawPath(path)

        if self.show_starting_message:
            painter.setPen(QColor.fromRgb(255, 255, 255, a=255))
            font = QFont()
            font.setPointSize(32)
            painter.setFont(font)
            painter.drawText(self.frameGeometry(), Qt.AlignCenter, 'Click and drag to select an area\nor right-click to select all the screen')

        painter.end()
        event.accept()

    def update_pixmap(self, filepath):
        self.screenshot_pixmap.load(filepath)

    def mouseMoveEvent(self, event):
        self.cursor_pos[0] = event.pos().x()
        self.cursor_pos[1] = event.pos().y()

        if bool(self.tool_widget_is_being_moved):
            self.update_controls_position(pos=[self.cursor_pos[0] - self.tool_widget_is_being_moved[0], self.cursor_pos[1] - self.tool_widget_is_being_moved[1]])

        elif self.is_cursor_pressing:
            if self.is_cursor_pressing == 'corner':
                if self.clip_rect.x() == self.clip_rect.right() and self.clip_rect.y() == self.clip_rect.bottom():
                    self.corner = ''
                    if event.pos().y() > self.clip_rect.y():
                        self.clip_rect.setBottom(event.pos().y())
                        self.corner += 'b'
                    else:
                        self.clip_rect.setTop(event.pos().y())
                        self.corner += 't'

                    if event.pos().x() > self.clip_rect.x():
                        self.clip_rect.setRight(event.pos().x())
                        self.corner += 'r'
                    else:
                        self.clip_rect.setLeft(event.pos().x())
                        self.corner += 'l'

                self.clip_rect_smaller_side = self.clip_rect.width() if self.clip_rect.width() < self.clip_rect.height() else self.clip_rect.height()

                if self.corner:
                    if self.clip_rect.width() < 0:
                        fx = self.clip_rect.x() + self.clip_rect.width()
                        fw = self.clip_rect.width() * -1
                        self.clip_rect.setLeft(fx)
                        self.clip_rect.setWidth(fw)
                    if self.clip_rect.height() < 0:
                        fx = self.clip_rect.y() + self.clip_rect.height()
                        fw = self.clip_rect.height() * -1
                        self.clip_rect.setTop(fx)
                        self.clip_rect.setHeight(fw)

                    if 'b' in self.corner:
                        bmargin = self.cursor_margin[1] if self.clip_rect_finished else 0
                        self.clip_rect.setBottom(event.pos().y() + bmargin if (event.pos().y() + bmargin) < self.height() else self.height())
                    if 't' in self.corner:
                        tmargin = self.cursor_margin[1] if self.clip_rect_finished else 0
                        self.clip_rect.setTop(event.pos().y() - tmargin if (event.pos().y() - tmargin) > 0 else 0)
                    if 'r' in self.corner:
                        rmargin = self.cursor_margin[0] if self.clip_rect_finished else 0
                        self.clip_rect.setRight(event.pos().x() + rmargin if (event.pos().x() + rmargin) < self.width() else self.width())
                    if 'l' in self.corner:
                        lmargin = self.cursor_margin[0] if self.clip_rect_finished else 0
                        self.clip_rect.setLeft(event.pos().x() - lmargin if (event.pos().x() - lmargin) > 0 else 0)

            elif self.is_cursor_pressing == 'annotation':
                if self.list_of_annotations:
                    if self.list_of_annotations[-1][0] in ['arrow', 'line', 'blur', 'square', 'filledsquare', 'circle', 'filledcircle', 'highlighter']:
                        if self.list_of_annotations[-1][1]:
                            self.list_of_annotations[-1][2] = [event.pos().x(), event.pos().y()]
                    elif self.list_of_annotations[-1][0] in ['freehand']:
                        self.list_of_annotations[-1][1].append([event.pos().x(), event.pos().y()])
        else:
            if self.tl_rect.contains(event.pos()):
                self.corner = 'tl'
            elif self.bl_rect.contains(event.pos()):
                self.corner = 'bl'
            elif self.br_rect.contains(event.pos()):
                self.corner = 'br'
            elif self.tr_rect.contains(event.pos()):
                self.corner = 'tr'
            elif self.l_rect.contains(event.pos()):
                self.corner = 'l'
            elif self.b_rect.contains(event.pos()):
                self.corner = 'b'
            elif self.r_rect.contains(event.pos()):
                self.corner = 'r'
            elif self.t_rect.contains(event.pos()):
                self.corner = 't'
            else:
                self.corner = False

        self.update()

    def mousePressEvent(self, event):
        if event.button() == Qt.RightButton:
            self.clip_rect = self.frameGeometry()
            self.show_starting_message = False
            self.update()
        else:
            if self.clip_rect.width() == 0 and self.clip_rect.height() == 0 and self.clip_rect.x() == 0 and self.clip_rect.y() == 0:
                self.clip_rect.setX(event.pos().x())
                self.clip_rect.setY(event.pos().y())
                self.clip_rect.setRight(event.pos().x())
                self.clip_rect.setBottom(event.pos().y())

            if not self.is_cursor_pressing:
                if self.list_of_annotations and not self.list_of_annotations[-1][1]:
                    self.is_cursor_pressing = 'annotation'
                    if self.list_of_annotations[-1][0] in ['arrow', 'line', 'blur', 'square', 'filledsquare', 'circle', 'filledcircle', 'highlighter']:
                        self.list_of_annotations[-1][1] = [event.pos().x(), event.pos().y()]
                    elif self.list_of_annotations[-1][0] in ['freehand']:
                        self.list_of_annotations[-1][1] = [[event.pos().x(), event.pos().y()]]
                elif self.clip_rect.contains(event.pos()):
                    self.is_cursor_pressing = 'corner'

            if self.corner:
                if 'r' in self.corner:
                    self.cursor_margin[0] = self.clip_rect.right() - event.pos().x()
                if 'l' in self.corner:
                    self.cursor_margin[0] = event.pos().x() - self.clip_rect.left()
                if 'b' in self.corner:
                    self.cursor_margin[1] = self.clip_rect.bottom() - event.pos().y()
                if 't' in self.corner:
                    self.cursor_margin[1] = event.pos().y() - self.clip_rect.top()

            if self.show_starting_message:
                self.show_starting_message = False

            if self.tool_widget_tools_options_move_button.underMouse():
                self.tool_widget_is_being_moved = [event.pos().x() - self.tool_widget.x(), event.pos().y() - self.tool_widget.y()]
                self.tool_widget_position_user_changed = True

            self.update_controls_position()

            self.update()

    def mouseReleaseEvent(self, event):
        self.corner = False
        self.is_cursor_pressing = False
        if not self.clip_rect_finished:
            self.clip_rect_finished = True
        self.tool_widget_is_being_moved = False
        self.update_controls_position()
        self.update()
        event.accept()

    def update_animations(self):
        if self.black_filter_alpha < 128:
            self.black_filter_alpha += 1
        self.update()


class about(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('About picd')

        self.setWindowIcon(QIcon(MAIN_ICON_PATH))
        self.setFixedSize(250, 300)

        layout = QVBoxLayout()
        layout.setContentsMargins(10, 20, 10, 20)
        layout.addStretch()

        self.icon_label = QLabel(self)
        self.icon_label.setPixmap(QIcon(MAIN_ICON_PATH).pixmap(64, 64))
        layout.addWidget(self.icon_label, alignment=Qt.AlignCenter)

        self.name_label = QLabel('picd', self)
        self.name_label.setStyleSheet('QLabel { font-size:22px; color:#304251; qproperty-alignment: "AlignCenter"; font-weight: bold;}')
        layout.addWidget(self.name_label)

        self.description_label = QLabel('<b>picd</b> is a software to take and share screenshots using the picd.in service', self)
        self.description_label.setStyleSheet('QLabel { font-size:14px; color:#304251; qproperty-alignment: "AlignCenter"; qproperty-wordWrap: true;}')
        layout.addWidget(self.description_label)

        self.version_label = QLabel('Version ' + VERSION_NUMBER, self)
        self.version_label.setStyleSheet('QLabel { font-size:10px; color:#6a7483; qproperty-alignment: "AlignCenter";}')
        layout.addWidget(self.version_label)
        layout.addStretch()

        self.setLayout(layout)

        frame_geometry = self.frameGeometry()

        screen = QGuiApplication.screenAt(QCursor.pos())
        center_point = screen.geometry().center()
        frame_geometry.moveCenter(center_point)
        self.move(frame_geometry.topLeft())


class preferences(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('picd Preferences')
        self.setWindowIcon(QIcon(MAIN_ICON_PATH))

        layout = QVBoxLayout(self)
        layout.setContentsMargins(10, 10, 10, 10)

        self.open_link_in_browser_checkbox = QCheckBox('Open link in browser', self)
        self.open_link_in_browser_checkbox.clicked.connect(self.copy_link_to_clipboard_clicked)
        layout.addWidget(self.open_link_in_browser_checkbox)

        self.copy_link_to_clipboard = QCheckBox('Copy link to clipboard', self)
        self.copy_link_to_clipboard.clicked.connect(self.copy_link_to_clipboard_clicked)
        layout.addWidget(self.copy_link_to_clipboard)

        self.upload_to_picdin = QCheckBox('Automatically upload to picd.in using command line', self)
        self.upload_to_picdin.clicked.connect(self.upload_to_picdin_clicked)
        layout.addWidget(self.upload_to_picdin)

        self.upload_to_picdin_on_confirm_button = QCheckBox('Automatically upload to picd.in clicking on "Confirm" button', self)
        self.upload_to_picdin_on_confirm_button.clicked.connect(self.upload_to_picdin_on_confirm_button_clicked)
        layout.addWidget(self.upload_to_picdin_on_confirm_button)

        # self.setLayout(layout)

        frame_geometry = self.frameGeometry()
        screen = QGuiApplication.screenAt(QCursor.pos())
        center_point = screen.geometry().center()
        frame_geometry.moveCenter(center_point)
        self.move(frame_geometry.topLeft())

    def update_preferences(self):
        self.open_link_in_browser_checkbox.setChecked(CONFIG['browser'].get('open_url_in_browser', True))
        self.copy_link_to_clipboard.setChecked(CONFIG['clipboard'].get('copy_url_to_clipboard', True))
        self.upload_to_picdin.setChecked(CONFIG['upload'].get('upload_to_picdin', True))
        self.upload_to_picdin_on_confirm_button.setChecked(CONFIG['upload'].get('upload_to_picdin_on_confirm', True))

    def open_link_in_browser_checkbox_clicked(self):
        CONFIG['browser']['open_url_in_browser'] = self.open_link_in_browser_checkbox.isChecked()

    def copy_link_to_clipboard_clicked(self):
        CONFIG['clipboard']['copy_url_to_clipboard'] = self.copy_link_to_clipboard.isChecked()

    def upload_to_picdin_clicked(self):
        CONFIG['upload']['upload_to_picdin'] = self.upload_to_picdin.isChecked()

    def upload_to_picdin_on_confirm_button_clicked(self):
        CONFIG['upload']['upload_to_picdin_on_confirm'] = self.upload_to_picdin_on_confirm_button.isChecked()


def visit_website():
    webbrowser.open('https://picd.in')


def systray(show_about_window=False, fire_on_init=False):
    app = QApplication(sys.argv)

    app.setQuitOnLastWindowClosed(False)
    app.setStyle('plastique')
    app.setApplicationName('picd')

    tray = QSystemTrayIcon()
    tray.setIcon(QIcon(os.path.join(PATH_PICD_GRAPHICS, 'picd_systray_win.png' if ACTUAL_OS == 'windows' else 'picd_systray.png')))
    tray.setVisible(True)

    menu = QMenu()

    # To take screenshot
    take = QAction("Take screenshot")
    menu.addAction(take)

    # To visit the website
    visit = QAction("Visit picd.in")
    visit.triggered.connect(visit_website)
    menu.addAction(visit)

    app.about = about()

    def open_about_screen():
        app.about.show()

    if show_about_window:
        open_about_screen()

    # To visit the website
    open_about = QAction("About picd")
    open_about.triggered.connect(open_about_screen)
    menu.addAction(open_about)

    app.preferences = preferences()

    def open_preferences_screen():
        app.preferences.update_preferences()
        app.preferences.show()

    # To visit the website
    open_preferences = QAction("Preferences...")
    open_preferences.triggered.connect(open_preferences_screen)
    menu.addAction(open_preferences)

    # To quit the app
    appquit = QAction("Quit")
    appquit.triggered.connect(app.quit)
    menu.addAction(appquit)

    tray.setContextMenu(menu)

    def blur_qpixmap(src, radius):
        scene = QGraphicsScene()
        item = QGraphicsPixmapItem()
        item.setPixmap(src)
        beffect = QGraphicsBlurEffect()
        beffect.setBlurRadius(radius)
        item.setGraphicsEffect(beffect)
        scene.addItem(item)
        res = QImage(src.size(), QImage.Format_ARGB32)
        res.fill(Qt.transparent)
        ptr = QPainter(res)
        scene.render(ptr)  # , QRect(), QRect(0,0, src.width(), src.height()) )
        # image = QPixmap.fromImage(res)
        return res

    app.fullscreen = fullscreen(app=app)

    def hotkeys_listener_received(command):
        if command == 'fire':
            if not app.fullscreen.isVisible():
                screenshot_filepath = screenshot.fire()
                app.fullscreen.update_pixmap(screenshot_filepath)
                app.fullscreen.reset()
                app.fullscreen.show()

            # response = upload.upload_to_picdin(filepath=screenshot_filepath)
            # if response and 'public_url' in response.json() and not app.fullscreen.isVisible():
            #     from picd.modules.config import CONFIG
            #     if CONFIG['browser'].get('open_url_in_browser', True):
            #         webbrowser.open(response.json()['public_url'])
            #         if CONFIG['clipboard'].get('copy_url_to_clipboard', True):
            #             app.clipboard().setText(response.json()['public_url'])
        if command == 'close':
            if app.fullscreen.isVisible():
                app.fullscreen.close()

    hotkeys_listener = listen_to_hotkeys()
    hotkeys_listener.command.connect(hotkeys_listener_received)
    hotkeys_listener.start()

    take.triggered.connect(lambda: hotkeys_listener_received(command='fire'))

    if fire_on_init:
        hotkeys_listener_received(command='fire')

    sys.exit(app.exec_())
