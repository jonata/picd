#!/usr/bin/env python3

import os
import pyscreenshot as ImageGrab

from picd.modules.paths import PATH_TMP


def fire():
    screenshot_filepath = os.path.join(PATH_TMP, "screenshot.png")
    result = False
    with ImageGrab.grab(childprocess=False) as im:
        im.save(screenshot_filepath)

    if os.path.isfile(screenshot_filepath):
        result = screenshot_filepath

    return result
