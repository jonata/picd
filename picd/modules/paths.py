#!/usr/bin/env python3

import os
import sys
import tempfile
import subprocess

import picd

PATH_PICD = os.path.dirname(picd.__file__)

PATH_HOME = os.path.expanduser("~")
REAL_PATH_HOME = PATH_HOME

STARTUPINFO = None

ACTUAL_OS = 'linux'

tempdir = tempfile.TemporaryDirectory()
PATH_TMP = tempdir.name

if sys.platform == 'darwin':
    ACTUAL_OS = 'macos'
    PATH_PICD_USER_CONFIG = os.path.join(PATH_HOME, 'Library', 'Application Support', 'picd')
elif sys.platform == 'win32' or os.name == 'nt':
    ACTUAL_OS = 'windows'
    PATH_PICD = os.path.abspath(os.path.dirname(sys.argv[0]))
    PATH_PICD_USER_CONFIG = os.path.join(os.getenv('LOCALAPPDATA'), 'picd')
    STARTUPINFO = subprocess.STARTUPINFO()
    STARTUPINFO.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    STARTUPINFO.wShowWindow = subprocess.SW_HIDE
else:
    try:
        PATH_REAL_HOME = subprocess.Popen(['getent', 'passwd', str(os.getuid())], stdout=subprocess.PIPE).stdout.read().decode().split(':')[5]
    except FileNotFoundError:
        pass
    if not os.path.isdir(os.path.join(PATH_HOME, '.config')):
        os.mkdir(os.path.join(PATH_HOME, '.config'))
    PATH_REAL_HOME = subprocess.Popen(['getent', 'passwd', str(os.getuid())], stdout=subprocess.PIPE).stdout.read().decode().split(':')[5]

    PATH_PICD_USER_CONFIG = os.path.join(PATH_HOME, '.config', 'picd')

PATH_PICD_GRAPHICS = os.path.join(PATH_PICD, 'graphics')

if not os.path.isdir(PATH_PICD_USER_CONFIG):
    os.mkdir(PATH_PICD_USER_CONFIG)

PATH_PICD_USER_CONFIG_FILE = os.path.join(PATH_PICD_USER_CONFIG, 'picd.config')

VERSION_NUMBER = '20.11.0.dev'
if os.path.isfile(os.path.join(PATH_PICD, 'current_version')):
    VERSION_NUMBER = open(os.path.join(PATH_PICD, 'current_version')).read().strip()


def get_graphics_path(filename):
    final_path = os.path.join(PATH_PICD_GRAPHICS, filename)
    if sys.platform == 'win32' or os.name == 'nt':
        final_path = final_path.replace('\\', '/')
    return final_path



