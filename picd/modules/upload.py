#!/usr/bin/env python3

import os
import requests
from picd.modules.config import CONFIG


def upload_to_picdin(filepath=False):
    result = False
    if filepath and CONFIG['upload'].get('upload_to_picdin', True):
        content_type = 'image/png'
        headers = {'content-type': content_type}
        url = 'https://picd.in/api/upload'
        img = open(os.path.join(filepath), 'rb').read()
        result = requests.post(url, data=img, headers=headers)
    return result
