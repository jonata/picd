#!/usr/bin/env python3

from PySide6.QtCore import QThread, Signal
from pynput import keyboard


class listen_to_hotkeys(QThread):
    command = Signal(str)

    def run(self):
        def on_press(key):
            if key == keyboard.Key.print_screen:
                self.command.emit('fire')

            if key == keyboard.Key.esc:
                self.command.emit('close')

        listener = keyboard.Listener(on_press=on_press)
        listener.start()
