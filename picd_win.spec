# -*- mode: python -*-

VERSION = '20.11.0.0'
if 'VERSION_NUMBER' in [*os.environ] and not os.environ['VERSION_NUMBER'] == '':
    VERSION = os.environ['VERSION_NUMBER']

a = Analysis(['picd/__main__.py'],
    pathex=['/Users/admin/Documents/picd'],
    datas=[
        ( 'picd/graphics/*', 'graphics' ),
    ],
    hiddenimports=['PySide2', 'pynput', 'pyscreenshot', 'requests', 'Pillow'],
    hookspath=[],
    runtime_hooks=[] )

pyz = PYZ(a.pure)

exe = EXE(pyz,
    a.scripts,
    exclude_binaries=True,
    name='picd.exe',
    strip=False,
    upx=True,
    console=False,
    debug=False,
    icon='picd/graphics/picd.ico' )

# exe_cmd = EXE(pyz,
#    a.scripts,
#     exclude_binaries=True,
#     name='picdc.exe',
#     strip=False,
#     upx=True,
#     console=True,
#     debug=True,
#     icon='picd/graphics/picd.ico' )


coll = COLLECT( exe, #  exe_cmd,
                a.binaries,
                a.zipfiles,
                a.datas,
                strip=False,
                upx=True,
                name='picd')

open('dist/picd/current_version', mode='w', encoding='utf-8').write(VERSION)
